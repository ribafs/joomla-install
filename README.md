# joomla-install
## Instalador do Joomla após feito o backup com o SimpleBackup

Este software é dos que gosto de criar, daqueles que automatizam tarefas manuais, tornando nossas vidas mais práticas.

Até hoje eu restaurei o backup do Joomla efetuando manualmente vários passos: criava manualmente o banco de dados, criava uma pasta, descompactava o arquivo zip na pasta e importava o sql no banco de dados. Agora tudo isso é feito por este pequeno software apenas informando os dados no formulário e clicando no botão Enviar.

Este software precisa dos arquivos zip e sql para instalar seu Joomla, ou seja, para restaurar o backup. A minha recomendação é que você use o componente SimpleBackup (https://github.com/ribafs/com_simplebackup) para gerar o backup na forma destes dois arquivos, mas você pode usar qualquer software, desde que ele gere os arquivos zip e sql.

Ajuda sobre o uso na index.html do software. Veja um exemplo de uso:
![Exemplo](./form.png)

Veja que este software pode ser utilizado não somente com Joomla mas com qualquer tipo de software, mesmo que não use PHP, com apenas algumas pequenas modificações.

## Agradecimentos
Acho importante agradecer a quem me facilitou a criação deste software:
- Twitter Bootstrap (me ajudou a criar um form mais elegante);
- Cricketlog Live em https://stackoverflow.com/questions/8889025/unzip-a-file-with-php/42756403#42756403 (descompactar zip);
- hugsbrugs em https://stackoverflow.com/questions/11901521/replace-string-in-text-file-using-php#11901576 (mudar variáveis no configuration.php);
- https://stackoverflow.com/questions/19751354/how-to-import-sql-file-in-mysql-database-using-php#19752106 (importar sql)
Sem vocês eu talvez não conseguisse ou se conseguisse levaria bem mais tempo para fazer. Grato a todos que compartilham seus conhecimentos através desta grande comunidade chamada Internet, que facilitam nossas vidas.

## Sugestões
Sugestões, dúvidas, críticas e avisos de erros favor acessar o Issues https://github.com/ribafs/joomla-install/issues e me mandar que agradeço.
